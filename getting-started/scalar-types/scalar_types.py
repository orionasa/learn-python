10
# 10
0b10
# 2
0o10
# 8
0x10
# 16
int(3.5)
# 3
int(-3.5)
# -3
int("200")
# 200
int("0101",2)
# 5
3.123
# 3.123
3e2
# 300
1.616e-35
# 1.616e-35
float(3)
# 3.0
float("4.34")
# 4.34
float("nan")
# nan
float("inf")
# inf
float("-inf")
# -inf
